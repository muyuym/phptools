<?php

class RotatingFile extends TestCase
{
    public function testDebug()
    {
        $log = new \Muyuym\Tools\Log\RotatingFile();
        $name = 'sat';
        $log->setName($name);
        $dir = dirname(__DIR__).'/build';
        $filename = sprintf('%s/%s/%s.log', $dir, $name, $name);
        $log->setFilename($filename);
        $log->debug('hello');
        $log->debug('world');
        $log->debug(['h' => 'hello', 'w' => 'world']);
        $obj = new stdClass();
        $obj->type = 'object';
        $obj->hello = 'hello world';
        $log->info($obj);
        $this->assertTrue(true);
    }
}
