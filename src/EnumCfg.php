<?php

namespace Muyuym\Tools;

class EnumCfg
{
    public static function cfg(): array
    {
        return [];
    }

    public static function nameCfg(): array
    {
        return array_column(static::cfg(), null, 'name');
    }

    public static function getNameById($id)
    {
        return isset(static::cfg()[$id]) ? static::cfg()[$id]['name'] : null;
    }

    public static function getIdByName($name)
    {
        return isset(static::nameCfg()[$name]) ? static::nameCfg()[$name]['id'] : null;
    }

    public static function contains($id): bool
    {
        return isset(static::cfg()[$id]);
    }

    public static function list()
    {
        return array_values(static::cfg());
    }

    public static function listById($id): array
    {
        return array_values(array_filter(static::cfg(), function ($item) use ($id) {
            return $item['id'] == $id;
        }));
    }
}
