<?php

namespace Muyuym\Tools\Log;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class RotatingFile
{
    protected $name;

    protected $filename;

    protected $maxFiles;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function setMaxFiles($maxFiles)
    {
        $this->maxFiles = $maxFiles;
    }

    public function error($data)
    {
        $this->log(Logger::ERROR, $data);
    }

    public function info($data)
    {
        $this->log(Logger::INFO, $data);
    }

    public function notice($data)
    {
        $this->log(Logger::NOTICE, $data);
    }

    public function debug($data)
    {
        $this->log(Logger::DEBUG, $data);
    }

    public function log($level, $data)
    {
        if ($data instanceof \Exception) {
            $message = $data->getMessage()."\nStack trace:\n".$data->getTraceAsString();
        } elseif (is_array($data) || is_object($data)) {
            $message = json_encode($data, JSON_UNESCAPED_UNICODE);
        } else {
            $message = $data;
        }
        $logger = new Logger($this->name);
        $logger->pushHandler($handler = new RotatingFileHandler($this->filename, (int) $this->maxFiles));
        $handler->setFormatter(new LineFormatter(null, 'Y-m-d H:i:s', true, true));
        $logger->log($level, $message);
    }
}
