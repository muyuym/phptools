<?php

namespace Muyuym\Tools\X;

class Rsa
{
    private false|\OpenSSLAsymmetricKey $private_key;
    private false|\OpenSSLAsymmetricKey $public_key;
    private int $sign_algo = OPENSSL_ALGO_SHA256;

    public function __construct(?string $private_key = null, ?string $public_key = null)
    {
        if (!extension_loaded('openssl')) {
            return;
        }
        $this->private_key = openssl_pkey_get_private($private_key);
        $this->public_key = openssl_pkey_get_public($public_key);
    }

    public function privateEncrypt(string $data): string
    {
        $encrypted_data = '';
        openssl_private_encrypt($data, $encrypted_data, $this->private_key);
        return base64_encode($encrypted_data);
    }

    public function privateDecrypt(string $data): string
    {
        $decrypted_data = '';
        openssl_private_decrypt(base64_decode($data), $decrypted_data, $this->private_key);
        return $decrypted_data;
    }

    public function publicEncrypt(string $data): string
    {
        $encrypted_data = '';
        openssl_public_encrypt($data, $encrypted_data, $this->public_key);
        return base64_encode($encrypted_data);
    }

    public function publicDecrypt(string $data): string
    {
        $decrypted_data = '';
        openssl_public_decrypt(base64_decode($data), $decrypted_data, $this->public_key);
        return $decrypted_data;
    }

    public function sign($data): string
    {
        $signature = '';
        openssl_sign($data, $signature, $this->private_key, $this->sign_algo);
        return base64_encode($signature);
    }

    public function verify($data, $signature): bool
    {
        return 1 == openssl_verify($data, base64_decode($signature), $this->public_key, $this->sign_algo);
    }

    public static function pemToBase64(string $data): string
    {
        if (!str_starts_with($data, '-----BEGIN')) {
            return $data;
        }
        $data = trim($data);
        $data = str_replace("\r\n", "\n", $data);
        $arr = explode("\n", $data);
        $arr = array_slice($arr, 1, -1);
        return implode('', $arr);
    }

    public static function base64ToPem(string $data, bool $private): string
    {
        if (str_starts_with($data, '-----BEGIN')) {
            return $data;
        }
        $which = $private ? 'PRIVATE' : 'PUBLIC';
        $pem = chunk_split($data, 64);
        return sprintf("-----BEGIN %s KEY-----\n%s-----END %s KEY-----", $which, $pem, $which);
    }
}
