<?php

namespace Muyuym\Tools\Structs;

class Result
{
    public int $code;

    public string $message;

    public array|object $data;

    public function __toString()
    {
        return json_encode($this);
    }
}
