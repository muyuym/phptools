<?php

namespace Muyuym\Tools;

class Sql
{
    const INSERT = 'insert';

    const UPDATE = 'update';

    /**
     * @param string $table
     * @param array $map
     * @param string $type
     * @param string $where
     * @return string
     */
    public static function sql_from_map(string $table, array $map, string $type = self::INSERT, string $where = ''): string
    {
        $type = strtolower($type);
        if (!in_array($type, [self::INSERT, self::UPDATE])) {
            return '';
        }
        if ($type == self::INSERT) {
            $k_arr = $v_arr = [];
            foreach ($map as $k=>$v) {
                $k_arr[] = "`$k`";
                $v_arr[] = "'$v'";
            }
            return sprintf('INSERT INTO `%s` (%s) VALUES (%s);', $table, implode(', ', $k_arr), implode(', ', $v_arr));
        } else {
            $set_arr = [];
            foreach ($map as $k=>$v) {
                $set_arr[] = sprintf('`%s` = \'%s\'', $k, $v);
            }
            $where = $where ? ' WHERE '.$where : '';
            return sprintf('UPDATE `%s` SET %s%s;', $table, implode(', ', $set_arr), $where);
        }
    }
}
