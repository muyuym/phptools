<?php

namespace Muyuym\Tools\Enums;

trait EnumTrait
{
    public static function names(): array
    {
        return array_map(fn($enum) => $enum->name, static::cases());
    }

    public static function values()
    {
        return array_map(fn($enum) => $enum->value, static::cases());
    }

    public static function fromName(string $name): static
    {
        foreach (self::cases() as $case) {
            if ($case->name === $name) {
                return $case;
            }
        }
        throw new \TypeError();
    }

    public static function tryFromName(string $name): ?static
    {
        foreach (self::cases() as $case) {
            if ($case->name === $name) {
                return $case;
            }
        }
        return null;
    }
}
