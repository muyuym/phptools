<?php

namespace Muyuym\Tools\Enums\EitherType;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;

/**
 * @template L
 * @template R
 *
 * @extends Either<L,R>
 */
class Right extends Either
{
    /**
     * @var R
     */
    private $value;

    /**
     * @param R $value
     *
     * @return void
     */
    private function __construct($value)
    {
        $this->value = $value;
    }

    public static function create($value)
    {
        return new self($value);
    }

    public function left(): Option
    {
        return None::create();
    }

    public function right(): Option
    {
        return Some::create($this->value);
    }

    /**
     * @param callable(R):R $f
     *
     * @return Either<L,R>
     */
    public function map(callable $f)
    {
        return self::create($f($this->value));
    }

    public function flatMap(callable $f)
    {
        return $f($this->value);
    }
}
