<?php

namespace Muyuym\Tools\Enums\EitherType;

use PhpOption\Option;

/**
 * @template L
 * @template R
 */
abstract class Either
{
    /**
     * @return Option<L>
     */
    abstract public function left(): Option;

    /**
     * @return Option<R>
     */
    abstract public function right(): Option;

    abstract public function map(callable $f);

    abstract public function flatMap(callable $f);
}
