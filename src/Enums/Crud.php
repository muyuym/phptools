<?php

namespace Muyuym\Tools\Enums;

enum Crud
{
    case Create;

    case Read;

    case Update;

    case Delete;
}
